<?php include('header.php');?>
<?php include('primari.php');?>
	<div id="banner-area">
			<!-- Subpage title start -->
			<div class="banner-title-content">
				<div class="container">
		        	<ul class="breadcrumb">
			            <li>Főoldal</li>
			            <li><a href="#"> Rólunk</a></li>
		          	</ul>
	          	</div>
          	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->

	<!-- Main container start -->

	<section id="main-container">
		<div class="container">

			<!-- About us -->
			<div class="row">
				<div class="col-md-12">
					<h2 class="article-title">Rólunk</h2>
				</div>
			</div><!-- Title row end -->

				<div class="article-content">
					<?php print_r($oldal->tartalom)?>
					<div class="gap-20"></div>

					<div class="who-we-box-wrapper">
						<div class="who-we-box one pulse">
							<h4 class="box-title"><strong>Gondos</strong> cégvezetés</h4>
						</div>
						<!--/ End box 1 -->
						<div class="who-we-box two pulse">
							<h4 class="box-title">Professzionális <strong>munkavégzés</strong></h4>
						</div>
						<!--/ End box 2 -->
						<div class="who-we-box three pulse">
							<h4 class="box-title">Az <strong>Ügyfél</strong> mindíg az első</h4>
						</div>
						<!--/ End box 3 -->
						<div class="who-we-box four pulse">
							<h4 class="box-title">Megbízható <strong>termékek</strong></h4>
						</div>
						<!--/ End box 4 -->
						<div class="who-we-box five pulse">
							<h4 class="box-title">Alacsony <strong>árak</strong>, piacon az első</h4>
						</div>
						<!--/ End box 5 -->
					</div>
				</div>
		</div><!--/ container end -->
			
	</section><!--/ Main container end -->
	


	<div class="gap-40"></div>
<?php include('footer.php');?>