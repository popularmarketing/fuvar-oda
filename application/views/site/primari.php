<body>
	<div class="body-inner">

	<!-- Header start -->
	<header id="header" class="ts-header">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 logo-wrapper">
					<!-- Logo start -->
					<div class="navbar-header">
					    <div class="navbar-brand">
						    <a href="<?php echo base_url();?>">
						    	<div class="logo" style="background-image: url('assets/uploads/files/<?php echo $beallitasok->logo;?>');"></div>
						    </a> 
					    </div>                   
					</div><!--/ Logo end -->
				</div>

				<div class="col-md-8 col-sm-6 col-xs-12">
					<!--
					<div class="consult">
						<a href="#"><i class="icon icon-mail3"></i> <span>Request Free Consultation</span></a>
					</div>
					-->
					<ul class="top-info">
						<li>
							<div class="info-box"><span class="info-icon"><i class="fa fa-phone">&nbsp;</i></span>
								<div class="info-box-content">
									<p class="info-box-title">Telefonszámunk</p>
									<p class="info-box-subtitle"><?php echo $beallitasok->mobil;?></p>
								</div>
							</div>
						</li>
						<li>
							<div class="info-box"><span class="info-icon"><i class="fa fa-compass">&nbsp;</i></span>
								<div class="info-box-content">
									<p class="info-box-title">Nyitvatartás</p>
									<p class="info-box-subtitle"><?php echo $beallitasok->nyitvatartas?></p>
								</div>
							</div>
						</li>
					</ul>
				</div>


			</div>
		</div>
	</header><!--/ Header end -->

	<!-- Navigation start -->
	<div class="navbar ts-mainnav">
		<div class="container">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </button>

			<nav class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
						
                            <?php foreach ($primari as $row) { ?>
                                <?php
                                $url = $row['cim'] == "" ? base_url("" . $row['url']) : $row['cim'];
                                if (empty($row['gyerekek'])) {
                                    ?>
                                    <li ><a href="<?php echo $url ?>"><?php echo $row['nev'] ?></a></li>                                      
                                <?php } else { ?>

                                    <li class="dropdown">
                                        <a href="<?php echo $url ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $row['nev'] ?><i class="fa fa-angle-down"></i></a>
										<div class="dropdown-menu">
											<ul>
												<?php
												foreach ($row['gyerekek'] as $gyRow) {

													$dropDownMenu = @count($gyRow['gyerekek']) > 0 ? "class='dropdown' " : "";
													$urlChild = $gyRow['cim'] == "" ? base_url("" . $gyRow['url']) : $gyRow['cim'];
													?>
													<li <?php echo $dropDownMenu ?>>
														<a href="<?php echo $urlChild ?>" <?php if($dropDownMenu != ""){?>class="dropdown-toggle" data-toggle="dropdown"<?php }?>><?php echo $gyRow['nev'] ?></a>
														<?php if ($dropDownMenu != "") { ?>
															<ul>
																<?php
																foreach ($gyRow['gyerekek'] as $gy2Row) {

																	$urlChild2 = $gy2Row['cim'] == "" ? base_url("" . $gy2Row['url']) : $gy2Row['cim'];
																	?>
																	<li>
																		<a href="<?php echo $urlChild2 ?>"><?php echo $gy2Row['nev'] ?></a>
																	</li>
																<?php } ?>
															</ul>
														<?php }
														?>
													</li>
												<?php } ?>
											</ul>
										</div>
                                    </li>
                                <?php } ?>
                            <?php } ?>
					</ul>
			</nav><!--/ Navigation end -->
		</div><!--/ Container end -->
	</div> <!-- Navbar end -->
