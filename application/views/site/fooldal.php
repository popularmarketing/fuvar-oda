<?php include('header.php')?>
<?php include('primari.php')?>
<?php include('slider.php')?>

     <!-- Feature area start -->
     <section id="features" class="ts-features no-padding">
     	<div class="container">
			<div class="row">
				<div class="col-md-12">

					<div class="widget feature-tab">
	                  	<ul class="nav nav-tabs">
		                    <li class="active"><a href="#tab1" data-toggle="tab">Röviden Rólunk</a></li>
	                  	</ul>
	                  	<div class="tab-content">
		                    <div class="tab-pane who-we active" id="tab1">
		                    	<p><img class="pull-left" src="images/tab/tab-1.png" alt="tab" /> </p>
		                    	<?php print_r($oldal->tartalom)?>
		                      </div><!-- Tab col end -->
						</div><!-- Content row end -->
					</div>
					<a class="btn btn-primary pull-right" href="rolunk">
						Többet Rólunk 
					</a>
				</div><!--/ Tab 1 end -->
	        </div><!-- End default tabs -->
		</div>
     </section> <!--/ Feature area end -->

	<!-- Feature box start -->
	<section id="feature" class="feature-img-icon no-padding" style="padding-bottom:20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="title-border"><strong>Miben</strong> Külömbözünk</h2>
				</div>
			</div> <!-- Title row end -->
			<div class="row">
				<div class="feature-img-icon-box col-md-3">
					<span class="feature-img">
						<img class="img-responsive" src="images/img-icon/1.png" alt="">
					</span>
					<div class="feature-img-content">
						<h3>Segítőkész munkatársak</h3>
						<p>Alkalmazottaink mindíg a rendelkezésükre állnak, ha segítségre van szükség.</p>
					</div>
				</div><!--/ End 1st Feature img icon-->

				<div class="feature-img-icon-box col-md-3">
					<span class="feature-img">
						<img class="img-responsive" src="images/img-icon/2.png" alt="">
					</span>
					<div class="feature-img-content">
						<h3>Fuvardíj engedmények</h3>
						<p>XVI. XIV. XV. XVII. kerületekbe fuvardíj engedménnyel szállítunk.</p>
					</div>
				</div><!--/ End 2nd Feature img icon-->

				<div class="feature-img-icon-box col-md-3">
					<span class="feature-img">
						<img class="img-responsive" src="images/img-icon/3.png" alt="">
					</span>
					<div class="feature-img-content">
						<h3>Megbízható termékek</h3>
						<p>Termékeink tiszták, minőségükben és árukban magasan a piac legjobb választásává váltak.</p>
					</div>
				</div><!--/ End 3rd Feature img icon-->

				<div class="feature-img-icon-box col-md-3">
					<span class="feature-img">
						<img class="img-responsive" src="images/img-icon/4.png" alt="">
					</span>
					<div class="feature-img-content">
						<h3>Egyenesen a gyártótól</h3>
						<p>Alapanyagainkat egyenesen a gyártótól szálítjuk, csak minimális profit százalék van rajta.</p>
					</div>
				</div><!--/ End 4th Feature img icon-->

				<div class="gap-30"></div>

				<div class="ts-general-btn text-center">
					<a class="btn btn-primary" href="rolunk">
						Többet Rólunk 
					</a>
				</div>
                                                  

			</div><!-- Content row end -->


		</div><!--/ Container end -->
	</section><!--/ Feature box end -->


	<section id="image-block" class="image-block no-padding">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 ts-padding img-block-left">
					<h2>Szolgáltatásaink</h2>
					<h3>Cégünk<strong> változatos</strong> szolgáltatásokat kínál.</h3>
					<p class="case-desc"><span class="cap">A</span> mellett, hogy minőségben és árban a piac tetején vagyunk, számtalan változatos szolgáltatást kínálunk. Alapanyagok szállítása mellett, még például a komposztokkal is foglalkozunk.</p>
					<div class="content-list">
						<h3>Fő szolgáltatásaink</h3>
						<ul class="list-arrow">
						<li>Föld szállítás</li>
						<li>Sóder szállítás</li>
						<li>Kavics szállítás</li>
						<li>Murva szállítás</li>
						</ul>
					</div>
					<p><a class="btn btn-primary btn-min-block" href="rolunk">Többet Rólunk</a></p>
			

				</div><!--/ End image block content -->

				<div class="col-md-6 ts-padding" style="height:600px;background:url(images/image-block-bg.jpg) 50% 50% / cover no-repeat;">
				</div>
			</div><!--/ Content row end -->
		</div><!--/ Container end -->
	</section><!--/ Image block end -->
<?php include("footer.php")?>