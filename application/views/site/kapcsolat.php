<?php include('header.php');?>
<?php include('primari.php');?>
	<div id="banner-area">
		<!-- Subpage title start -->
		<div class="banner-title-content">
			<div class="container">
	        	<ul class="breadcrumb">
		            <li>Főoldal</li>
		            <li><a href="#"> Kapcsolat</a></li>
	          	</ul>
          	</div>
      	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->

	<!-- Main container start -->

	<section id="main-container">
		<div class="container">
		

			<div class="row">
	    		<div class="col-md-6">
	    			<h3 class="title-normal">Írjon nekünk</h3>
	    			<form id="contact-form" action="oldal/sendmail" method="post" role="form">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Név</label>
								<input class="form-control" name="senderName" id="name" placeholder="" type="text" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Email</label>
									<input class="form-control" name="email" id="email" 
									placeholder="" type="email" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Tárgy</label>
									<input class="form-control" name="subject" id="subject" 
									placeholder="" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Üzenet</label>
							<textarea class="form-control" name="message" id="message" placeholder="" rows="10" required></textarea>
						</div>
						<div class="text-right"><br>
							<button class="btn btn-primary solid blank" type="submit">Üzenet küldése</button> 
						</div>
					</form>
	    		</div>
	    		<div class="col-md-6">
	    			<h3 class="title-normal">Elérési lehetőségek</h3>

	    			<div class="contact-info">
			    		<?php print_r($oldal->tartalom)?>
			    		<br>
			    		<p><i class="fa fa-map-marker"></i> <?php echo $beallitasok->uzletcim?> </p>
						<p><i class="fa fa-phone info"></i>  <?php echo $beallitasok->mobil?> </p>
						<p><i class="fa fa-envelope-o info"></i>  <?php echo $beallitasok->nyilvanosemail?></p>
						<p><i class="fa fa-globe info"></i>  <?php echo base_url();?></p>
    				</div>
	    		</div>
	    	</div>

		</div><!--/ container end -->

	</section><!--/ Main container end -->
	

	<div class="gap-40"></div>
<?php include('footer.php');?>