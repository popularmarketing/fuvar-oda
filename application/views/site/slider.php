<!-- Slider start -->
	<section id="home" class="no-padding">	
    	<div id="main-slide" class="cd-hero">
			<ul class="cd-hero-slider">
			<?php foreach($slider->result() as $row){?>
				<li class="selected">
					<div class="image-bg">
						<img class="" src="assets/uploads/slider/<?php echo $row->file?>" alt="slider">
					</div>
					<div class="cd-full-width left-align">
					  	<?php print_r($row->leiras)?>
					  	<a href="rolunk" class="btn btn-primary solid cd-btn">Rólunk</a>
					</div> 
				</li><!--/ slide1 -->
			<?php }?>
			</ul> <!--/ cd-hero-slider -->
		</div><!--/ Main slider end -->    	
    </section> <!--/ Slider end -->