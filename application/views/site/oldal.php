<?php include('header.php');?>
<?php include('primari.php');?>
	<div id="banner-area">
			<!-- Subpage title start -->
			<div class="banner-title-content">
				<div class="container">
		        	<ul class="breadcrumb">
			            <li>Főoldal</li>
			            <li><a href="#"> <?php echo $oldal->nev?></a></li>
		          	</ul>
	          	</div>
          	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->

	<!-- Main container start -->

	<section id="main-container">
		<div class="container">

			<!-- About us -->
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<?php print_r($oldal->tartalom)?>
				</div><!--/ Content col 8 end -->

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="sidebar sidebar-right">

						<div class="widget solid dark">
							<h3 class="widget-title">Kapcsolat</h3>
							<div class="widget-wrapper">
								<h4>Nyitvatartás</h4>
								<?php print_r($beallitasok->nyitvatartas)?>
								<p><a href="kapcsolat">Küldjön nekünk üzenetet <i class="fa fa-angle-right">&nbsp;</i></a></p>
								<h4>Telefonszám</h4>
								<p class="ph-no"><?php echo $beallitasok->mobil?></p>
							</div>

						</div><!-- Widget end -->

					</div><!--/ Sidebar end -->
				</div><!--/ Content col 4 end -->

			</div><!--/ Content row end -->



		</div><!--/ container end -->
			
	</section><!--/ Main container end -->
	


	<div class="gap-40"></div>
<?php include('footer.php');?>