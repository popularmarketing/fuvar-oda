
	<!-- Footer start -->
	<footer id="footer" class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12 footer-widget">
					<div class="footer-logo">
						<img src="assets/uploads/files/<?php echo $beallitasok->logo?>" alt="" />
					</div>

					<ul class="footer-social unstyled">
						<li>
							<a title="RSS" href="#">
								<span class="social-icon"><i class="fa fa-rss"></i></span>
							</a>
							<a title="Facebook" href="#">
								<span class="social-icon"><i class="fa fa-facebook"></i></span>
							</a>
							<a title="Twitter" href="#">
								<span class="social-icon"><i class="fa fa-twitter"></i></span>
							</a>
							<a title="Google+" href="#">
								<span class="social-icon"><i class="fa fa-google-plus"></i></span>
							</a>
							<a title="linkedin" href="#">
								<span class="social-icon"><i class="fa fa-linkedin"></i></span>
							</a>
							<a title="Pinterest" href="#">
								<span class="social-icon"><i class="fa fa-pinterest"></i></span>
							</a>
							<a title="Dribble" href="#">
								<span class="social-icon"><i class="fa fa-dribbble"></i></span>
							</a>
						</li>
					</ul>
	
				</div><!--/ End Recent Posts-->
				

				<div class="col-md-3 col-sm-12 footer-widget footer-about-us">
					<h3 class="widget-title"><strong>Lépjen</strong> Kapcsolatba</h3>

					<h4><i class="fa fa-map-marker">&nbsp;</i> Üzlet címe</h4>
					<p><?php echo $beallitasok->uzletcim?></p>
							<h4><i class="fa fa-envelope-o">&nbsp;</i> Email</h4>
							<p><?php echo $beallitasok->nyilvanosemail?></p>
							<h4><i class="fa fa-phone">&nbsp;</i> Telefon</h4>
							<p><?php echo $beallitasok->mobil?></p>
				</div><!--/ end About us -->


				<div class="col-md-3 col-sm-12 footer-widget">
					<h3 class="widget-title"><strong>Fontos</strong> Linkek</h3>

					<ul class="list-arrow">
						<li><a href="fooldal">Főoldal</a></li>
						<li><a href="rolunk">Rólunk</a></li>
						<li><a href="kapcsolat">Kapcsolat</a></li>
					</ul>
			
				</div><!--/ end Industrial market -->

				<div class="col-md-3 col-sm-12 footer-widget">
					<h3 class="widget-title"><strong>Földdel</strong> Kapcsolatos Oldalak</h3>

						<ul class="list-arrow">
							<li><a href="#">Darált föld</a></li>
							<li><a href="#">Komposzt</a></li>
							<li><a href="#">Rostált föld</a></li>
							<li><a href="#">Trágyás föld</a></li>
						</ul>
					
				</div><!--/ end Industrial market -->

			</div><!-- Row end -->
		</div><!-- Container end -->

		<!-- Copyright start -->
		<section id="copyright" class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 copyright-info">
						Copyright © 2016 Minden jog fenntartva - <a href="http://popularmarketing.hu/" target="_blank" style="color:orange;">Weboldal készítés <img src="img/logo.png" height="50px"></a>
					</div>

				</div><!--/ Row end -->
			   <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
					<button class="btn btn-primary" title="Back to Top">
						<i class="fa fa-angle-double-up"></i>
					</button>
				</div>
			</div><!--/ Container end -->
		</section><!--/ Copyright end -->

	</footer><!-- Footer end -->
	

	<!-- Javascript Files
	================================================== -->

	<!-- initialize jQuery Library -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<!-- Bootstrap jQuery -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- Style Switcher -->
	<script type="text/javascript" src="js/style-switcher.js"></script>
	<!-- Owl Carousel -->
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<!-- Bxslider -->
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<!-- CD Hero slider -->
	<script type="text/javascript" src="js/cd-hero.js"></script>
	<!-- Isotope -->
	<script type="text/javascript" src="js/isotope.js"></script>
	<script type="text/javascript" src="js/ini.isotope.js"></script>
	<!-- Eeasing -->
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<!-- Counter -->
	<script type="text/javascript" src="js/jquery.counterup.min.js"></script>
	<!-- Waypoints -->
	<script type="text/javascript" src="js/waypoints.min.js"></script>
	<!-- Template custom -->
	<script type="text/javascript" src="js/custom.js"></script>
	
	</div><!-- Body inner end -->
</body>
</html>
<!-- Localized -->