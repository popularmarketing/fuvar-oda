-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2016 at 04:50 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `beallitasok`
--

CREATE TABLE IF NOT EXISTS `beallitasok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'Alap oldal', '', '', '', '', '', '', '', '', '', '', '', 1, 'hu', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `companytype`
--

CREATE TABLE IF NOT EXISTS `companytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dokumentumok`
--

CREATE TABLE IF NOT EXISTS `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  PRIMARY KEY (`dokumentumokid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gyarto`
--

CREATE TABLE IF NOT EXISTS `gyarto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gyik`
--

CREATE TABLE IF NOT EXISTS `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hirek`
--

CREATE TABLE IF NOT EXISTS `hirek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hirek_kategoria`
--

CREATE TABLE IF NOT EXISTS `hirek_kategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategoria`
--

CREATE TABLE IF NOT EXISTS `kategoria` (
  `kategoriaid` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`kategoriaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategoriak`
--

CREATE TABLE IF NOT EXISTS `kategoriak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kerdezzfelelek`
--

CREATE TABLE IF NOT EXISTS `kerdezzfelelek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `marka`
--

CREATE TABLE IF NOT EXISTS `marka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `megjelenes`
--

CREATE TABLE IF NOT EXISTS `megjelenes` (
  `megjelenesid` int(11) NOT NULL AUTO_INCREMENT,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`megjelenesid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `nyelvi_forditasok`
--

CREATE TABLE IF NOT EXISTS `nyelvi_forditasok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `oldalak`
--

CREATE TABLE IF NOT EXISTS `oldalak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=52 ;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `termekek`
--

CREATE TABLE IF NOT EXISTS `termekek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `termek_kepek`
--

CREATE TABLE IF NOT EXISTS `termek_kepek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `termek_tulajdonsagok`
--

CREATE TABLE IF NOT EXISTS `termek_tulajdonsagok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tulajdonsag`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tulajdonsag_kat`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag_kat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
